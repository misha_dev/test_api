<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name'  => $faker->word,
        'cost'  => $faker->numberBetween(1000, 200000),
        'weight'  => $faker->numberBetween(0.2, 100),
    ];
});
