<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::prefix('/user')->group(function(){
    Route::post('/login', 'api\AuthController@login');
    Route::post('/register', 'api\AuthController@register');
});

Route::middleware('auth:api')->group(function(){
    Route::get('/payment_history', 'api\PaymentHistoryController@index');
    Route::post('/product_buy', 'api\ProductController@buy');
});
