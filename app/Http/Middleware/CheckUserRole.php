<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class CheckUserRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->role == User::INVALID_USER) return response()->json(['message' => 'You cant do it']);

        return $next($request);
    }
}
