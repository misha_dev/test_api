<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Process\RedisConnection;;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ProductController extends Controller
{
    public function buy(Request $request)
    {
        $user_id = auth()->user()->id;
        if(Cache::get('user_id') != $user_id){
            RedisConnection::connect($request, $user_id);
            Cache::put('user_id', $user_id, 60);
            return response()->json(['message' => 'Your purchase in progress']);
        } return response()->json(['message' => 'Wait a minute']);
    }

}
