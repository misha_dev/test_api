<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $loginData = $this->validateLogin($request);
        if($loginData->fails())
        {
            return response()->json(['errors'=>$loginData->errors()], 422);

        } elseif(!Auth::attempt($request->all()))
        {
            return response()->json(['message' => 'Incorrect login or password']);
        }

        $accessToken = Auth::user()->createToken('authToken')->accessToken;
        return response()->json([
            'user' => Auth::user(),
            'access_token'  => $accessToken,
        ]);
    }

    public function register(Request $request)
    {
        $registerData = $this->validateRegistration($request);

        if($registerData->passes()) {
            $user = new User;

            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->balance = 10000;
            $user->role = 1;

            $user->save();

            $accessToken = $user->createToken('authToken')->accessToken;

            return response()->json([
                'user' => $user,
                'access_token' => $accessToken,
            ]);
        } else return response()->json(['errors' => $registerData->errors()], 422);
    }

    public function validateLogin(Request $request)
    {
        return Validator::make($request->all(),[
           'email'    => 'required|string',
           'password' => 'required|string',
        ]);
    }

    public function validateRegistration(Request $request)
    {
        return Validator::make($request->all(),[
           'name'  => 'required|string',
           'email'  => 'required|string|unique:users',
           'password'  => 'required|string',
        ]);
    }
}
