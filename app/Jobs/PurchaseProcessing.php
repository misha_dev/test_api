<?php

namespace App\Jobs;

use App\PaymentHistory;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PurchaseProcessing implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $request;
    protected $user_id;
    protected $cost;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request, $user_id)
    {
        $this->request = $request;
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = User::find($this->user_id);
        $cost = $this->countTheCost($this->request);
        if($cost < $user->balance) {
            foreach ($this->request['products'] as $item) {
                $payment_history = new PaymentHistory();
                $payment_history->product_id = $item['product_id'];
                $payment_history->amount = $item['amount'];
                $payment_history->user_id = $this->user_id;
                $payment_history->save();
            }
            $user->balance -= $cost;
            $user->save();
        }
    }

    public function countTheCost($request)
    {
        $cost = 0;
        foreach ($request['products'] as $product)
        {
            $data = Product::find($product['product_id']);
            $cost += $data->cost;
            if($product['amount'] > 1) $cost *= $product['amount'];
        }
        return $cost;
    }
}
