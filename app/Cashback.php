<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cashback extends Model
{
    protected $fillable = ['user_id', 'product_id', 'sum_of_cashback', 'payment_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function paymenthistory()
    {
        return $this->belongsTo(PaymentHistory::class);
    }
}
