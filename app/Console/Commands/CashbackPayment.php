<?php

namespace App\Console\Commands;

use App\Cashback;
use App\User;
use Illuminate\Console\Command;

class CashbackPayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cashback:user {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get the cashback for all user product';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::where('email', $this->argument('email'))->first();
        foreach ($user->paymenthistory as $payment) {
                if(!$payment->cashback) {
                    $cashback = $payment->product->cost * 0.05;
                    Cashback::create([
                        'user_id' => $user->id,
                        'sum_of_cashback' => $cashback,
                        'payment_id' =>$payment->id,
                    ]);
                    $user->balance += $cashback;
                    $user->save();
                }
            }
        $this->line("Cashback counted");
    }
}
