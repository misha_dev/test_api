<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'cost', 'weight'];

    public function paymenthistory()
    {
        return $this->hasMany(PaymentHistory::class, 'product_id');
    }
}
