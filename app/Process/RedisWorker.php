<?php


namespace App\Process;

use App\PaymentHistory;
use App\Product;
use App\User;
use Illuminate\Support\Facades\Redis;

class RedisWorker
{
    public static function run()
    {
        $redis = Redis::connection();

        while (true) self::transformRedisData($redis);

        $redis->close();
    }

    public static function transformRedisData($redis)
    {
        $redis_data = $redis->zrange('payment', 0, -1);
        if(!empty($redis_data)) {
            foreach ($redis_data as $item) {
                $user_id = $redis->hget($item, "user_id");
                $payment_data = json_decode($redis->hget($item, "data"), true);
                $cost = self::countTheCost($payment_data['products']);
                if (self::checkUserBalance($user_id, $cost)) {
                    self::changeUserBalance($user_id, $cost);
                    self::addToDB($payment_data['products'], $user_id);
                    $redis->del($item);
                    $redis->del('payment');
                }
            }
        } else return false;
    }

    public static function addToDB($payment_array, $user_id)
    {
        foreach ($payment_array as $payment){
            PaymentHistory::create([
                'product_id' => $payment['product_id'],
                'amount' => $payment['amount'],
                'user_id' => $user_id,
            ]);
        }
    }

    public static function changeUserBalance($user_id, $cost)
    {
        $user = User::find($user_id);
        $user->balance -= $cost;
        $user->save();
    }


    public static function checkUserBalance($user_id, $cost)
    {
        $user = User::find($user_id);
        if($user->balance >= $cost) return true;
        return false;
    }

    public static function countTheCost($payment_array)
    {
        $cost = 0;
        foreach ($payment_array as $item){
            $cost += Product::find($item['product_id'])->cost;
            if($item['amount'] > 1) $cost *= $item['amount'];
        }

        return $cost;
    }
}
