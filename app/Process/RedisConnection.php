<?php

namespace App\Process;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Str;

class RedisConnection
{
    public static function connect(Request $request, $user_id)
    {
        $redis = Redis::connection();

        $token = Str::random(50);

        $redis->hset("product:token:{$token}", "user_id", $user_id);
        $redis->hset("product:token:{$token}", "data", json_encode($request->all()));

        $redis->zadd("payment",'NX', time(), "product:token:{$token}");

        $redis->close();
    }
}
